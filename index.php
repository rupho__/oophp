<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP dalam PHP</title>
</head>
<body>

<?php

require "./animal.php";
require_once "./ape.php";
require_once "./frog.php";


echo "<h1>Release 0</h1>";
$sheep = new Animal("Shaun");
echo  $sheep->name;
echo "<br>" . $sheep->legs;
echo "<br>" . $sheep->cold_blooded;

echo "<h1>Release 1</h1>";
$sungokong = new Ape("Kera Sakti");
echo $sungokong->name;
echo "<br>".$sungokong->legs;
echo $sungokong->yell();
echo "<br><br>" ;

$kodok = new Frog("buduk");
echo  $kodok->nameFrog;
echo  $kodok->name;
echo "<br>" . $kodok->legs;
$kodok->jump();
?>
    
</body>
</html>