<?php

require_once "./animal.php";


class Frog extends Animal{

    public $nameFrog;
    public function __construct($name)
    {
        $this->name = $name;
    }

    public $legs = 4;

    function jump(){
        echo "<br>Hop Hop";
    }

}